package arrayassignment2;

import java.util.*;

public class AddTwoMatrices {

	public static void main(String[] args) {
		int matrix1[][] = new int[2][2];
		int matrix2[][] = new int[2][2];
		int sum[][] = new int[2][2];
		Scanner sc = new Scanner(System.in);
		System.out.println("enter elements into matrix");
		for (int i = 0; i < matrix1.length; i++) {
			for (int j = 0; j < matrix2.length; j++) {
				matrix1[i][j] = sc.nextInt();
			}
		}
		System.out.println("enter elements into another matrix");
		for (int i = 0; i < matrix1.length; i++) {
			for (int j = 0; j < matrix2.length; j++) {
				matrix2[i][j] = sc.nextInt();
			}
		}
		for (int i = 0; i < matrix1.length; i++) {
			for (int j = 0; j < matrix2.length; j++) {
				sum[i][j] = matrix1[i][j] + matrix2[i][j];

				System.out.print(" " + sum[i][j]);
			}
			System.out.println();
		}
	}

}
