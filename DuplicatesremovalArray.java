package arrayassignment2;

import java.util.Scanner;

public class DuplicatesremovalArray {

	public static void main(String[] args) {
		int[] array = new int[5];
		int i, j = 0;
		int temp[] = new int[array.length];
		int len = array.length;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter elements into array :");

		for (i = 0; i < len; ++i) {
			array[i] = sc.nextInt();
		}
		System.out.println("Before removing duplicate elements array is :");

		for (i = 0; i < len; ++i) {
			System.out.println(array[i]);
		}

		System.out.println("After removing duplicate elements array is :");
		for (i = 0; i < len - 1; ++i) {
			if (array[i] != array[j]) {
				temp[j] = array[i];
				j++;
			}

		}
		temp[j] = temp[array.length - 1];
		for (i = 0; i < temp.length; ++i) {
			System.out.println(temp[i]);
		}
	}

}